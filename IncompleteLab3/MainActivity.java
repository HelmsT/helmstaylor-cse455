package com.example.a005918779.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
    String url;
    private static final String url = "https://api.fixer.io/latest?base=USD";
    String json = "";
    String line = "";
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //cast the variables to their ids
        editText01 = findViewByID(R.id.EditText01);
        bnt01 = findViewByID(R.id.bnt);
        textView01 = findViewByID>(R.id.Yen);



        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen)  {
            double convert = Double.parseDouble(json);
            usd = editText01.getText().toString();
            if (usd.equals("")){
                textView01.setText("This field cannot be blank!");
            } else {
                Double dInputs = Double.parseDouble(usd);
                Double result = dInputs * convert;
                textView01.setText("$" + usd + " = " + "Y" + String.format("%.2f", result));
                editText01.setText("");
            }
                }
            }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {super.onPreExecute(); }
        @Override
        protected void onProgressUpdate(Void... values)
        @Override
        protected void OnPostExecute(String result){
            super.onPostExecute(result);
            intln("\nWhat is rate:")
            Double value = Double.parseDouble(rate);
                usd = editText01.getText().toString();
                //if-else statement to make sure user cannot leave the EditText blank
                if (usd.equals("")){
                    textView01.setText("This field cannot be blank!");
                } else {
                    //Convert string to double
                    Double dInputs = Double.parseDouble(usd);
                    //Convert function
                    Double result = dInputs * 112.57;
                    //Display the result
                    textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));
                    //clear the edittext after clicking
                    editText01.setText(""); }
        }
    @Override
    protected String doInBackground(Void... params) {
        try {
            URL web_url = new URL(MainActivity.this.url);
            HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection);
            httpURLConnection.setRequestMethod("GET");
            System.out.println("\nTESTING ... BEFORE connection method to URL\n");
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input))
            System.out.println("CONNECTION SUCCESSFUL\n");

            while (line != null){
                line = bufferedReader.readline();
                json += line;
            }
            System.out.println("\nTHE JSON: " + json);
            JSONObject obj = new JSONObject(json);
            JSONObject objRate = obj.getJSONObject("rates");
            rate = objRate.get("JPY").toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(tag:"MYAPP", msg:"unexpected JSON exception", e);
            System.exit(status1);
        }
        return null;
    }
    }}}